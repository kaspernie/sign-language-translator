import withAuth from "../hoc/withAuth";
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory";
import { storageRead, storageSave } from "../utils/storage";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { NavLink } from "react-router-dom";
import { useUser } from "../context/UserContext";

const Profile = () => {
  const { setUser } = useUser();
  const logout = () => {
    storageSave(STORAGE_KEY_USER, null);
    console.log("logout clicked");
    setUser(null);
  };

  return (
    <>
      <h1>Profile page</h1>
      <NavLink to="/translation">
        <i
          className="fa fa-language fa-2x"
          style={{
            position: "absolute",
            top: "0.75em",
            left: "80%",
            color: "white",
          }}
        ></i>
      </NavLink>
      <div className="inputContainer2">
        <h4>10 latest translations:</h4>
        <ul>
          <ProfileTranslationHistory />
        </ul>
      </div>
      <button name="logout" onClick={logout}>
        Logout
      </button>
    </>
  );
};
export default withAuth(Profile);
