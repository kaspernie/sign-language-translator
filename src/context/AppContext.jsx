import UserProvider from "./UserContext";

const AppContext = ({ children }) => {
  // children desctructered from props
  return <UserProvider>{children}</UserProvider>;
};
export default AppContext;
