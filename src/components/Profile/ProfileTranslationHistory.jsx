import { render } from "@testing-library/react";
import { useEffect, useState } from "react";
import { storageRead, storageSave } from "../../utils/storage";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { deleteTranslation } from "../../api/translation";
import { loginUser } from "../../api/user";
import { useUser } from "../../context/UserContext";

const ProfileTranslationHistory = () => {
  const [translation, setTranslation] = useState([]);
  const { user, setUser } = useUser();

  useEffect(() => {
    console.log(user);
    setTranslation(user.translations);
  });

  const handleDelete = async () => {
    setTranslation([]);
    const [errorDelete, result] = await deleteTranslation(
      user // storageRead(STORAGE_KEY_USER)
    );
    const [error, userResponse] = await loginUser(
      user //storageRead(STORAGE_KEY_USER).username
    );
    storageSave(STORAGE_KEY_USER, userResponse);
    setUser(userResponse);
  };
  if (translation.length > 10) {
    return (
      <div>
        {translation.slice(-10).map((trans) => (
          <li>{trans}</li>
        ))}
        <button onClick={handleDelete}>Delete</button>
      </div>
    );
  } else if (translation.length > 0) {
    return (
      <div>
        {translation.map((trans, i) => (
          <li>{translation[i]}</li>
        ))}
        <button onClick={handleDelete}>Delete</button>
      </div>
    );
  } else
    return (
      <div>
        <p>No translations</p>
      </div>
    );
};

export default ProfileTranslationHistory;
