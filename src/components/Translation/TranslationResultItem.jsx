const TranslationResultItem = ({ letter }) => {
  const imgPath = `assets/images/individial_signs/${letter}.png`;
  return (
    <li style={{ display: "inline-block" }}>
      <img src={imgPath} alt={letter} width="50" />
    </li>
  );
};

export default TranslationResultItem;
