import { useForm } from "react-hook-form";

const inputTextConfig = {
  required: true,
  maxLength: 40,
};

const TranslationForm = ({ onTranslate }) => {
  const {
    register: translate,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = ({ inputText }) => {
    onTranslate(inputText);
  };

  const errorMessage = (() => {
    if (!errors.inputText) {
      return null;
    }
    if (errors.inputText.type === "required") {
      return (
        <span style={{ display: "block", paddingTop: "55px" }}>
          Please enter text to translate!
        </span>
      );
    }
    if (errors.inputText.type === "maxLength") {
      return (
        <span style={{ display: "block", paddingTop: "55px" }}>
          Text too long to translate. Please enter a maximum of 40 characters.
        </span>
      );
    }
  })();

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="inputContainer">
          <label htmlFor="inputText"></label>
          <input
            type="text"
            {...translate("inputText", inputTextConfig)}
            placeholder="hello"
          />
          {errorMessage}
          <button type="submit" className="translateButton">
            {/* &nbsp;&#10097;&nbsp; */}
            {/* &#10140; */}
            <i className="fa fa-arrow-right"></i>
          </button>
        </div>
      </form>
    </>
  );
};
export default TranslationForm;
