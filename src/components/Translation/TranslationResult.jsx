import TranslationResultItem from "./TranslationResultItem";

const TranslationResult = ({ text }) => {
  const textArr = text.split("");
  const translatedText = textArr.map((char) => {
    return <TranslationResultItem letter={char} />;
  });

  return (
    <section style={{ backgroundColor: "#ffc75f" }}>
      <h3>Translated text:</h3>
      <ul style={{ listStyleType: "none" }}>{translatedText}</ul>
    </section>
  );
};

export default TranslationResult;
